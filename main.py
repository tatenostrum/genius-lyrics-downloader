from bs4 import BeautifulSoup
import requests
import codecs
import Queue
import threading


exitFlag = 0
workQueue = Queue.Queue()
queueLock = threading.Lock()
threads = []


class MyThread(threading.Thread):

    def __init__(self, threadid, rq):
        threading.Thread.__init__(self)
        self.threadID = threadid
        self.rq = rq

    def run(self):
        print "Starting " + str(self.threadID)
        scraper(self.rq)
        print "Exiting " + str(self.threadID)


def scraper(rq):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            data = rq.get()
            queueLock.release()
            try:
                req = requests.get(data.split("\n")[0])
                soup = BeautifulSoup(req.text, "lxml")
                lyric = soup.find("div", class_="lyrics")
                lyric = lyric.find("p")
                lyrics = unicode(lyric.text)
                flyrics = codecs.open(
                    str(soup.find("h1").text) + " - " + str(soup.find("h2").text).split("\n")[1] + ".txt",
                    'w')
                flyrics.write(lyrics.encode('utf-8'))
            except TypeError:
                raise Exception("The URL don't exists")
            finally:
                flyrics.close()
        else:
            queueLock.release()


def fillqueue():
    urls = open("urls.txt", 'r')
    queueLock.acquire()
    for i in urls:
        workQueue.put(i)
    queueLock.release()
    urls.close()


if __name__ == '__main__':
    for a in range(0, 5):
        thread = MyThread(a, workQueue)
        thread.start()
        threads.append(thread)
    fillqueue()
    while not workQueue.empty():
        pass
    exitFlag = 1
    for t in threads:
        t.join()
