# Genius Lyrics downloader

Lyrics downloader for https://genius.com/

## Required

* Python 2.7
* BeautifulSoup4
* lxml

## Instructions

The program needs a file that contains the URL of the lyrics that you want download. The file must be named "urls.txt" and the urls must be separated by separated lines.

Then, execute the program and the lyrics are saved in the same directory.

## urls.txt example:
```
https://genius.com/Tommy-lee-sparta-nuh-make-me-feel-suh-lyrics
https://genius.com/Tommy-lee-sparta-hero-maybe-lyrics
https://genius.com/Tommy-lee-sparta-redemption-song-lyrics
https://genius.com/Tommy-lee-sparta-psycho-lyrics
```